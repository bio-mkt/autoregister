#include <File.au3>
#include <FileConstants.au3>
#include <Math.au3>

;open email, name files
$file_email = "Resource/emails.txt"
$file_name = "Resource/Names.txt"

FileOpen($file_email, 0)
FileOpen($file_name, 0)

;get email, name counts from files
$nameCount = _FileCountLines($file_name)
$emailCount = _FileCountLines($file_email)

$count = _Min($nameCount, $emailCount)
$maxCount = _Max($nameCount, $emailCount)

Local  $name[$maxCount + 1];
Local  $email[$maxCount + 1];

;save them in array
For $i = 1 to $count
    $tmp = FileReadLine($file_name, $i)
    $name[$i] = $tmp

	$tmp = FileReadLine($file_email, $i)
	$email[$i] = $tmp;
Next

For $i = 1 to $count
   $sName = ""
   For $j = 1 To 10
	   $sName &= Chr(Random(48, 57, 1))
   Next

   ShellExecute("Application\chrome.exe" , "--profile-directory=""Profile 1""" & $sName &" https://prayer.rhapsodyofrealities.org/signup.php")
   Sleep(1000)

   WinWait("Rhapsody Prayer Network - Google Chrome")
   WinMove("Rhapsody Prayer Network - Google Chrome","",0,0,1024,768)

   Sleep(2000)
   Send("{tab}")
   Send($name[$i])
   Sleep( Random(300, 600) )

   Send("{tab}");
   Send($email[$i])
   Sleep( Random(300, 600) )

   Send("{tab}");
   Send("12345")
   Sleep( Random(300, 600) )

   Send("{tab}");
   Send("{space}")
   Sleep( Random(300, 600) )

   Send("{tab}");
   Sleep( Random(300, 600) )

   Send("{tab}");
   Send("{space}")
   Sleep( Random(300, 600) )

   Sleep(1000)

   WinClose("Rhapsody Prayer Network - Google Chrome")
Next
